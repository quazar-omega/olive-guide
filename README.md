# [Olive guide (Unofficial)](https://quazar-omega.gitlab.io/olive-guide)

A work in progress guide to illustrate how to use the [Olive video editor](https://olivevideoeditor.org/) version 0.2, particularly to better document the node system

## Warning
This guide could become unreliable really often since Olive 0.2 is in heavy development making the information here deprecated, the purpose of this guide is to be possibly useful to those adventurous enough to try out this software during its alpha stages

## How you can help
I'm not a professional video editor and I my knowledge of Olive itself is quite limited, so if anyone is willing to make suggestions/additions, don't hesitate to either open issues or pull requests